unit MainGameBoard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdDice, StdCtrls, YazRules, Scoreboard, Vcl.Buttons;

type
  TfrmMainGameBoard = class(TForm)
    Dice1: TStandardDice;
    Dice2: TStandardDice;
    Dice3: TStandardDice;
    Dice4: TStandardDice;
    Dice5: TStandardDice;
    btnRollDice: TButton;
    btnStopDice: TButton;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    lblUpperTotal: TLabel;
    lblLowerTotal: TLabel;
    lblGrandTotal: TLabel;
    btnOnes: TButton;
    lbltot1: TLabel;
    lbltot6: TLabel;
    lbltot5: TLabel;
    lbltot4: TLabel;
    lbltot3: TLabel;
    lbltot2: TLabel;
    lblUpperTotalCalc: TLabel;
    lblThreeOfKindTot: TLabel;
    lblFourOfKindTot: TLabel;
    lblSmallStraightTot: TLabel;
    lblLargeStraightTot: TLabel;
    lblYazTot: TLabel;
    lblChanceTot: TLabel;
    lblLowerTot: TLabel;
    lblGrandTot: TLabel;
    btnTwos: TButton;
    btnThrees: TButton;
    btnFours: TButton;
    btnFives: TButton;
    btnSixes: TButton;
    btnThreeOfKind: TButton;
    btnFourOfKind: TButton;
    btnSmallStraight: TButton;
    btnLargeStraight: TButton;
    btnYaz: TButton;
    btnChance: TButton;
    btnRestartGame: TButton;
    lblFullHouse: TLabel;
    btnFullHouse: TButton;
    BitBtn1: TBitBtn;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnRollDiceClick(Sender: TObject);
    procedure btnStopDiceClick(Sender: TObject);
    procedure Dice1Click(Sender: TObject);
    procedure Dice1DblClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnRestartGameClick(Sender: TObject);
    procedure btnOnesClick(Sender: TObject);
    procedure btnTwosClick(Sender: TObject);
    procedure btnThreesClick(Sender: TObject);
    procedure btnFoursClick(Sender: TObject);
    procedure btnFivesClick(Sender: TObject);
    procedure btnSixesClick(Sender: TObject);
    procedure btnThreeOfKindClick(Sender: TObject);
    procedure btnFourOfKindClick(Sender: TObject);
    procedure btnFullHouseClick(Sender: TObject);
    procedure btnSmallStraightClick(Sender: TObject);
    procedure btnLargeStraightClick(Sender: TObject);
    procedure btnYazClick(Sender: TObject);
    procedure btnChanceClick(Sender: TObject);
  private
    FDiceSet: TDiceSet;
    FScoreBoard: TScoreboard;
    fnDiceTotal: Integer;
    procedure SetDiceSet(const Value: TDiceSet);
    procedure SetScoreboard(const Value: TScoreboard);
    procedure Lock_Unlock(const oDice: TStandardDice; nValue: Integer);
    procedure CheckLockBtn;
    procedure ResetLock;
    { Private declarations }
  public
    property DiceSet: TDiceSet read FDiceSet write SetDiceSet;
    property Scoreboard: TScoreboard read FScoreBoard write SetScoreboard;
    property nDiceTotal: Integer read fnDiceTotal write FnDiceTotal;
    procedure ResetScoreboard;
    procedure UpdateScoreboard;
    { Public declarations }
  end;

var
  frmMainGameBoard: TfrmMainGameBoard;

implementation

{$R *.dfm}

procedure TfrmMainGameBoard.FormDestroy(Sender: TObject);
begin
  if Assigned(DiceSet)then DiceSet.Free();
  if Assigned(Scoreboard) then Scoreboard.Free();

end;

procedure TfrmMainGameBoard.ResetScoreboard;
begin
  // Reset scoreboard
  nDiceTotal := 0;
  btnOnes.Enabled := True;
  btnTwos.Enabled := True;
  btnThrees.Enabled := True;
  btnFours.Enabled := True;
  btnFives.Enabled := True;
  btnSixes.Enabled := True;
  btnThreeOfKind.Enabled := True;
  btnFourOfKind.Enabled := True;
  btnSmallStraight.Enabled := True;
  btnLargeStraight.Enabled := True;
  btnFullHouse.Enabled := True;
  btnYaz.Enabled := True;
  btnChance.Enabled := True;

  lbltot1.Caption := '0';
  lbltot2.Caption := '0';
  lbltot3.Caption := '0';
  lbltot4.Caption := '0';
  lbltot5.Caption := '0';
  lbltot6.Caption := '0';
  lblThreeOfKindTot.Caption := '0';
  lblFourOfKindTot.Caption := '0';
  lblFullHouse.Caption := '0';
  lblYazTot.Caption := '0';
  lblSmallStraightTot.Caption := '0';
  lblLargeStraightTot.Caption := '0';
  lblChanceTot.Caption := '0';
  lblUpperTotalCalc.Caption := '0';
  lblLowerTot.Caption := '0';
  lblGrandTot.Caption := '0';
end;

procedure TfrmMainGameBoard.FormCreate(Sender: TObject);
begin
  DiceSet := TDiceSet.Create();
  Scoreboard := TScoreboard.Create();
end;

procedure TfrmMainGameBoard.btnChanceClick(Sender: TObject);
begin
  Scoreboard.setScore(stChance, DiceSet);
end;

procedure TfrmMainGameBoard.btnFivesClick(Sender: TObject);
begin
  Scoreboard.setScore(stFives, DiceSet);
end;

procedure TfrmMainGameBoard.btnFourOfKindClick(Sender: TObject);
begin
  Scoreboard.setScore(stFourOfKind, DiceSet);
end;

procedure TfrmMainGameBoard.btnFoursClick(Sender: TObject);
begin
  Scoreboard.setScore(stFours, DiceSet);
end;

procedure TfrmMainGameBoard.btnFullHouseClick(Sender: TObject);
begin
  Scoreboard.setScore(stFullHouse, DiceSet);
end;

procedure TfrmMainGameBoard.btnLargeStraightClick(Sender: TObject);
begin
  Scoreboard.setScore(stLargeStraight, DiceSet);
end;

procedure TfrmMainGameBoard.ResetLock;
var i: Integer;
    oCompo, oBtn: TComponent;
begin
  for I := 1 to 5 do
  begin
    oCompo := FindComponent('Dice' + I.ToString);
    oBtn := FindComponent('SpeedButton' + I.ToString);
    (oCompo as TStandardDice).Locked := False;
    (oBtn as TSpeedButton).Font.Color := clBlack;
    (oBtn as TSpeedButton).Enabled := True;
  end;
end;

procedure TfrmMainGameBoard.btnOnesClick(Sender: TObject);
begin
  Scoreboard.setScore(stOnes, DiceSet);
  lbltot1.Caption := Scoreboard.Ones.ToString;
  ResetLock;
end;

procedure TfrmMainGameBoard.btnRestartGameClick(Sender: TObject);
begin
  ResetScoreBoard;
end;

procedure TfrmMainGameBoard.CheckLockBtn;
var I: Integer;
    oCompo, oBtn: TComponent;
begin
  for I := 1 to 5 do
  begin
    oCompo := FindComponent('Dice' + I.ToString);
    oBtn := FindComponent('SpeedButton' + I.ToString);
    if (oCompo as TStandardDice).Locked then
    begin
      (oBtn as TSpeedButton).Font.Color := clRed;
      (oBtn as TSpeedButton).Enabled := false;
    end
    else
      (oBtn as TSpeedButton).Font.Color := clBlack;
  end;
end;

procedure TfrmMainGameBoard.btnRollDiceClick(Sender: TObject);
begin
  if nDiceTotal < 3  then
  begin
    CheckLockBtn;
    nDiceTotal := nDiceTotal + 1;
    Dice1.Rotate := True;
    Dice2.Rotate := True;
    Dice3.Rotate := True;
    Dice4.Rotate := True;
    Dice5.Rotate := True;
  end;
end;

procedure TfrmMainGameBoard.btnSixesClick(Sender: TObject);
begin
  Scoreboard.setScore(stSixes, DiceSet);
end;

procedure TfrmMainGameBoard.btnSmallStraightClick(Sender: TObject);
begin
  Scoreboard.setScore(stSmallStraight, DiceSet);
end;

procedure TfrmMainGameBoard.btnStopDiceClick(Sender: TObject);
begin
  Dice1.Rotate := False;
  Dice2.Rotate := False;
  Dice3.Rotate := False;
  Dice4.Rotate := False;
  Dice5.Rotate := False;
end;

procedure TfrmMainGameBoard.btnThreeOfKindClick(Sender: TObject);
begin
  Scoreboard.setScore(stThreeOfKind, DiceSet);
end;

procedure TfrmMainGameBoard.btnThreesClick(Sender: TObject);
begin
  Scoreboard.setScore(stThrees, DiceSet);
end;

procedure TfrmMainGameBoard.btnTwosClick(Sender: TObject);
begin
  Scoreboard.setScore(stTwos, DiceSet);
end;

procedure TfrmMainGameBoard.btnYazClick(Sender: TObject);
begin
  Scoreboard.setScore(stYaz, DiceSet);
end;

procedure TfrmMainGameBoard.Lock_Unlock(const oDice: TStandardDice; nValue: Integer);
begin
  oDice.Locked := not oDice.Locked;
  case nValue of
    1: DiceSet.Value1 := oDice.Value;
    2: DiceSet.Value2 := oDice.Value;
    3: DiceSet.Value3 := oDice.Value;
    4: DiceSet.Value4 := oDice.Value;
    5: DiceSet.Value5 := oDice.Value;
  end;
end;

procedure TfrmMainGameBoard.Button1Click(Sender: TObject);
Var oLoc_Dice: TComponent;
    nValue: Integer;
begin
  nValue := (Sender as TSpeedButton).tag;
  oLoc_Dice := FindComponent('Dice' + nValue.ToString);
  if Assigned(oLoc_Dice) then
    Lock_Unlock((oLoc_Dice as TStandardDice), nValue);
end;

procedure TfrmMainGameBoard.Dice1Click(Sender: TObject);
begin
  (Sender as TStandardDice).Locked := True;
end;

procedure TfrmMainGameBoard.Dice1DblClick(Sender: TObject);
begin
  (Sender as TStandardDice).Locked := False;
end;

procedure TfrmMainGameBoard.SetDiceSet(const Value: TDiceSet);
begin
  FDiceSet := Value;
end;

procedure TfrmMainGameBoard.SetScoreboard(const Value: TScoreboard);
begin
  FScoreBoard := Value;
end;

procedure TfrmMainGameBoard.UpdateScoreboard;
begin
//
end;

end.
